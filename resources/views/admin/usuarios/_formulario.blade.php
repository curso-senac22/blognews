@csrf

{{-- @if ($errors -> any())

@foreach ($errors->all() as $erro )
<div>
    {{$erro }}
</div>

@endforeach

@endif --}}

<div class="col-md-12">
    <label for="nome" class="form-label">Nome</label>
    <input type="text" class="form-control @error('nome') is-invalid @enderror" name="nome" id="nome" placeholder="Insira o Nome" value="{{ old('nome')}}">


@error('nome')

    <div class="invalid-feedback">
        {{$message}}
    </div>

@enderror


<div class="col-md-12">
    <label for="email" class="form-label">E-mail</label>
    <input type="text"  class="form-control @error('email') is-invalid @enderror" name="email" class="form-control" id="email" placeholder="Insira a E-mail" value="{{ old('nome')}}>

    @error('email')

    <div class="invalid-feedback">
        {{$message}}
    </div>

@enderror


</div>
<div class="col-md-12">
    <label for="senha" class="form-label">Senha</label>
    <input type="password"   class="form-control @error('password') is-invalid @enderror" name="password" class="form-control" id="senha" placeholder="">

    @error('senha')

    <div class="invalid-feedback">
        {{$message}}
    </div>

@enderror
</div>

<div class="col-md-3">
    <label for="role" class="form-label">Nível</label>
    <select class="form-control" id="role" name="role">
        <option value="Editor" {{old('role')== 'Editor' ? 'selected': ''}}>Editor</option>
        <option value="Administrador" {{old('role')== 'Administrador' ? 'selected': ''}}>Administrador</option>
    </select>
</div>

<div class="col-12">
    <button type="submit" class="btn btn-primary">Salvar</button>
</div>
